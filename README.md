# How to use this repository

This is the Repository linking to my blog post on medium : [Seamlessly integrated deep learning environment](https://medium.com/p/faee4b351e94)

## Create a GCP project and a service account

```
gcloud projects create <your>-dl --enable-cloud-apis
gcloud config set project  <your>-dl
gcloud services enable compute.googleapis.com
gcloud iam service-accounts create gcp-terraform-dl --display-name gcp-terraform-dl
gcloud projects add-iam-policy-binding  <your>-dl \
            --member='serviceAccount:gcp-terraform-dl@<your>-dl.iam.gserviceaccount.com' --role='roles/owner'

gcloud iam service-accounts keys create 'credentials.json' --iam-account='gcp-terraform-dl@<your>-dl.iam.gserviceaccount.com'
```

Replace <your> with your desired project id.

## Request Quota

Unfortunately you have to request a quota for the GPU (it was working for me until Dec. 2018 without it). Please follow the advise from this [Stackoverflow article](https://stackoverflow.com/questions/45227064/how-to-request-gpu-quota-increase-in-google-cloud). The handling of this request can take up to 2 days.


## Install terraform
```
brew install terraform
```

### Optional if you want to deploy and store some code
Only if you want to deploy a repository with some code to the machine.

#### Create a ssh key and add it to your start_up_script.sh
```
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
```

* Get the private key and add replace with it the placehold in `ADD_YOUR_SSH_KEY_HERE` in `start_up_script.sh`

#### Create a gitlab repository you want to deploy <optional>

* Create the gitlab repo
* Change the `start_up_script.sh` to use your repository
* Add the public ssh key to your gitlab user

## Roll out the changes
```
terraform init
terraform apply \
  -var 'project_id=<your>-dl' \
  -var 'region=europe-west1-d'
```
Replace `<your>`

## Destroy the machine

```
terraform destroy \
  -var 'project_id=<your>-dl' \
  -var 'region=europe-west1-d'
```

(Disclaimer: this works also with github or any other git repo)
